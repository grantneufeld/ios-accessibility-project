# iOS Accessibility Project: Testing

As this particular repository is just focused on text documentation so far,
the only thing to test is the format of the documentation.

**[mdl](https://github.com/markdownlint/markdownlint) (MarkdownLint)**
checks formatting and style issues (linting) of Markdown (`.md`) text files.

The style configuration for this project is in the `.markdown_style.rb` file in the project root.

You will need to install mdl for your system.
On macOS you can use [Homebrew](https://brew.sh/)
(which you will need to install if you haven’t already).

```sh
brew install mdl
```

To run mdl, from the project root directory:

```sh
mdl --warnings .
```
