# iOS Accessibility Project: Mission

**DRAFT**.

This document outlines the vision of *why* we are doing this project,
the goals we are pursuing to further that vision,
as well as the actions we are trying in the hope of achieving those goals.

## Ideal (vision)

That all iOS apps be as fully accessible as possible.

## Working Goals (outcomes)

* Have developers, designers, and other decision makers, see—and act on—the value in prioritizing
  accessibility.
* Have people with diverse accessibility needs actively included in the design and testing of apps.
* Have developers, and others involved in designing and producing apps,
  understand design principles and technologies for enabling accessibility.

## Strategies

* Expand the resources available for learning how to make iOS apps accessible;
  especially in the forms of documentation and tutorials.
* Expand the number of developers who know accessibility design techniques and technologies.
* Expand the number of people (not just developers) with the skills to perform accessibility
  audits of apps.
* Develop a set of principles, guidelines, and documentation, for an Accessible-First Development
  approach specific to creating iOS apps.

## Tactics (possibile actions, ideas)

* Gather developers to collaborate on retrofitting existing app code to be accessible.
  This is to be a shared learning experience, and produce documentation and tutorials on
  those processes.
* Have workshops on the various available accessibility technologies and design techniques
  to increase developer understanding and skills.
* Have workshops by people with disabilities to increase developer understanding of
  accessibility needs, and create space for collaborations.
* Have online meetups, bringing together people with disabilities, developers, and designers,
  to look at current impediments in technologies and collaborate to think up better approaches.
