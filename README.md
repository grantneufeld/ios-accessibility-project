# iOS Accessibility Project

A collaborative-learning project to increase accessibility of iOS apps.

**DRAFT**.

* [Mission Statement](./meta/mission.md): Our vision, goals, strategies, and tactics.
* [Code of Conduct](./code_of_conduct.md)

This is open to iOS developers of all skill levels and backgrounds.

To start with, we’re planning to try retrofitting accessibility into an existing open-source iOS
app (TBD) that wasn’t designed for accessibility—producing some documentation and tutorials in the
process. This is in the hope of helping more developers improve the accessibility of their apps.

Everything here is open to change.
So, if you disagree with something, or otherwise think there could be a better way,
please join the discussion to let us know your views.

## Getting involved

Please let us know if there are ways to better include you and others
for whom some of the communication tools we are using so far
(Zoom, Slack, Discord, GitLab, Twitter, etc.) have barriers to participation.
Suggestions for better (more inclusive) tools, or add-ons to those tools, especially welcome.

We welcome pull requests to refine and add to the documentation here.

You can also find us on the `#accessibility-project` channel on the iOS Dev Happy Hour Slack server.
(Contact [Grant](https://gitlab.com/grantneufeld) about joining if you don’t have access yet.)

We’ll likely be having some online workshops/meetups, too.

All participants are expected to act within the [Code of Conduct](./code_of_conduct.md) here.

## Contributing

We’re currently using [Markdown](https://en.wikipedia.org/wiki/Markdown) (`.md`) format
for the documentation here.
There is documentation on [using the mdl linting tool](./meta/testing.md)
to validate the format of Markdown files in this project.

You may contribute in other formats if those better suit the documentation.
Please do try to use formats that can be readily adapted/converted/modified.
(I.e., PDF is problematic.)

Please aim for plain language when reasonable.
If jargon or other complex terms are necessary, please try to include a definition inline,
or a glossary.

## Authors and acknowledgments

* [Grant Neufeld](https://gitlab.com/grantneufeld) ([@grant on Twitter](https://twitter.com/grant))

This project started with a discussion in an “accessibility” break out room at the
[iOS Dev Happy Hour](https://www.iosdevhappyhour.com/) on January 22, 2022.

## License

Copyright ©2022.

The documents in this project are licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).

This means you can freely copy the documents here. But, because this is a collaborative project,
if you make changes or improvements, you’re expected to share them, too, so we all benefit.

This license doesn’t allow you to profit off of these materials
(although you are certainly encouraged to profit off of what you hopefully learn here).
If you want to use any of this material in a commercial context,
please contact us about a fair license agreement.
